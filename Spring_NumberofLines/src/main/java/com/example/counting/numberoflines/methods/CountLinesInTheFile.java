package com.example.counting.numberoflines.methods;

import com.example.counting.numberoflines.customexception.CustomException;

import java.io.IOException;
import java.io.Reader;

public class CountLinesInTheFile {
    GetValuesFromConfigFile getValuesFromConfigFile = new GetValuesFromConfigFile();
    public CountLinesInTheFile(){
        // Private constructor to prevent instantiation
    }
    public int countingLinesInTheFile(String content) {
        return (int) content.chars().filter(ch -> ch == '\n').count();
    }
    public int countingLinesInTheFile(Reader reader) {
        char [] cout = new char[1024];
        int numChar = 0;
        int count =0;
        while (numChar != -1){
            try {
                numChar = reader.read(cout,0,1024);
            } catch (IOException e) {
                throw new CustomException(getValuesFromConfigFile.getErrorMessageWhileReadingTheContentOfTheFile(),e);
            }
            if(numChar !=-1){
            for(char charCount:cout){
                if(charCount == '\n')
                    count++;
            }
            }
        }
        return count;
    }
}
