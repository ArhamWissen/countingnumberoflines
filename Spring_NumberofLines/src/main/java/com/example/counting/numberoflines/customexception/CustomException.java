package com.example.counting.numberoflines.customexception;

public class CustomException extends RuntimeException{

    public CustomException(String message,Throwable e){
        super(message, e);
    }
}
